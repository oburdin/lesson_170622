from selenium import webdriver
from pages.wiki_article import *
from pages.wiki_img import *
from pages.wiki_start import *
from selenium.webdriver.common.by import By
import time
import pytest

class TestWiki():
    params = (["Cletus", "список римських пап"], ["Pope", "список римських пап"], ["капуцин", "список мавп"])

    def setup(cls):
        cls.driver = webdriver.Chrome()

    def teardown(cls):
        cls.driver.close()

    @pytest.mark.parametrize(('text_to_find', 'search_text'),
                             (params))
    def test_find_pope(self, text_to_find, search_text):
        # Создние вебдрайвера и переход на начальную страницу
        driver = self.driver
        driver.get("https://uk.wikipedia.org/")

        # Поиск поля поиска и ввод запроса для поиска
        start_page = WikiStart(driver)
        start_page.click_search_field().fill_in_search_field(search_text).submit_search()


        # Поиск нужной картинки и нажатие на ссылку в которой хранится картинка
        wiki_article = WikiArticle(driver)
        wiki_article.click_the_link_below_image()

        # Проверка, есть ли на открытой странице текст, который является критерием прохождения теста
        wiki_img_page = WikiImg(driver)
        assert wiki_img_page.is_text_on_page(text_to_find)
