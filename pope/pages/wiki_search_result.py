from pages import sample_page


class WikiSearchResult(sample_page.SmaplePage):
    def find_element_xpath(self, xpath):
        return self.browser.find_element_by_xpath(xpath)