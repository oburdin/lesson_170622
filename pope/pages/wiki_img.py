from selenium.webdriver.common.by import By

from pages import sample_page


class WikiImg(sample_page.SmaplePage):
    cletus_caption_locator = (By.XPATH, "/html/body/div[8]/div/div[3]/div[1]/p/span[1]/a[1]")

    def is_text_on_page(self, text_to_find):
        self.wait_for_element(self.cletus_caption_locator)
        if self.browser.find_elements(*self.cletus_caption_locator):
            return True
        else:
            return False