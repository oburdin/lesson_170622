from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

class SmaplePage():
    def __init__(self, testsetup):
        self.browser = testsetup

    def find_element(self, locator):
        return self.browser.find_element(*locator)

    def click_on_element(self, element):
        element.click()
        return self

    def clear_clear_field(self, element):
        element.clear()
        return self

    def input_keys_to_field(self, input_text, element):
        element.send_keys(input_text)
        return self

    def submit_field(self, element):
        element.submit()
        return self

    def wait_for_element(self, locator_BY):
        WebDriverWait(self.browser, 5).until(EC.presence_of_element_located(locator_BY))
        return self

    def scroll_to_element(self, element):
        actions = ActionChains(self.browser)
        actions.move_to_element(element).perform()