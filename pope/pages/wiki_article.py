from selenium.webdriver.common.by import By

from pages import sample_page


class WikiArticle(sample_page.SmaplePage):
    search_img_locator = (By.XPATH, "//a/img[@class='Файл:3-St.Cletus']")
    def click_the_link_below_image(self):
        self.wait_for_element(self.search_img_locator)
        self.find_element(self.search_img_locator).click()
        return self