from selenium.webdriver.common.by import By

from pages import sample_page


class WikiStart(sample_page.SmaplePage):

    search_field_locator = (By.XPATH, "//input[@id='searchInput']")
    search_button = (By.XPATH, "//input[@id='searchButton']")
    def click_search_field(self):
        self.wait_for_element(self.search_field_locator)
        self.find_element(self.search_field_locator).click()
        return self

    def fill_in_search_field(self, text):
        self.find_element(self.search_field_locator).send_keys(text)
        return self

    def submit_search(self):
        self.find_element(self.search_button).click()
        return self




